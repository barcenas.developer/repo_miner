import Config

config :repo_miner_web, RepoMinerWebWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: RepoMinerWebWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: RepoMinerWe.PubSub,
  live_view: [signing_salt: "8p+9EyGA"]

config :repo_miner_web, RepoMinerWebWeb.Endpoint,
  # Binding to loopback ipv4 address prevents access from other machines.
  # Change to `ip: {0, 0, 0, 0}` to allow access from other machines.
  http: [ip: {127, 0, 0, 1}, port: 4000],
  check_origin: false,
  code_reloader: true,
  debug_errors: true,
  secret_key_base: "kZMCCb8A5/9mwv+hApeN+rkUNbbs8QwVwIMV4+DOp7rxGXiX4bo+3plgZHdOVTs6"
