defmodule RepoMinerCoreWebWeb.CodeRepoServiceFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `RepoMinerCore.CodeRepoService` context.
  """

  @doc """
  Generate a repository.
  """
  def repository_fixture(attrs \\ %{}) do
    {:ok, repository} =
      attrs
      |> Enum.into(%{
        name: "some name",
        token: "some token",
        url: "some url",
        visibility: :public
      })
      |> RepoMinerCore.Services.CodeRepoService.create_repository()

    repository
  end

  def provider_fixture(attrs \\ %{}) do
    {:ok, provider} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> RepoMinerCore.Services.CodeRepoService.create_provider()

    provider
  end

  def commit_density_fixture(attrs \\ %{}) do
    {:ok, commit_density} =
      attrs
      |> Enum.into(%{
        year: 2022,
        month: 5,
        no_commits: 12
      })
      |> RepoMinerCore.Services.CodeRepoService.create_commit_density()

    commit_density
  end

  def branches_fixture(attrs \\ %{}) do
    {:ok, branch} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> RepoMinerCore.Services.CodeRepoService.create_branch()

    branch
  end
end
