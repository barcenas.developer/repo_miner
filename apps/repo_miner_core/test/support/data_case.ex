defmodule RepoMinerCore.DataCase do
  @moduledoc false
  use ExUnit.CaseTemplate

  using do
    quote do
      alias RepoMinerCore.Repo

      import Ecto
      import Ecto.Query
      import RepoMinerCore.DataCase
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(RepoMinerCore.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(RepoMinerCore.Repo, {:shared, self()})
    end

    :ok
  end
end
