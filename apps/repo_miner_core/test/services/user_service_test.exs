defmodule UserServiceTest do
  # use ExUnit.Case
  use RepoMinerCore.DataCase
  alias RepoMinerCore.Services.UserService

  describe "users" do
    alias RepoMinerCore.Models.User
    import RepoMinerCoreWebWeb.UserServiceFixtures

    @invalid_attrs %{name: nil, username: nil, email: nil, role: nil}

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert UserService.list_users() == [user]
    end

    test "get_user!/1 returns the user with the given id" do
      user = user_fixture()
      assert UserService.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      valid_attrs = %{
        name: "some name",
        username: "some username",
        email: "some email",
        role: :admin
      }

      assert {:ok, %User{} = user} = UserService.create_user(valid_attrs)
      assert user.name == "some name"
      assert user.username == "some username"
      assert user.email == "some email"
      assert user.role == :admin
    end

    test "create_user/1 with invalid data returns error" do
      assert {:error, %Ecto.Changeset{}} = UserService.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()

      update_attrs = %{
        name: "updated name",
        username: "updated username",
        email: "updated email",
        role: :collaborator
      }

      assert {:ok, %User{} = user} = UserService.update_user(user, update_attrs)

      assert user.name == "updated name"
      assert user.username == "updated username"
      assert user.email == "updated email"
      assert user.role == :collaborator
    end

    test "update_user/2 with invalid data returns error" do
      user = user_fixture()

      assert {:error, %Ecto.Changeset{}} = UserService.update_user(user, @invalid_attrs)

      assert user == UserService.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = UserService.delete_user(user)

      assert_raise Ecto.NoResultsError, fn ->
        UserService.get_user!(user.id)
      end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = UserService.change_user(user)
    end
  end
end
