defmodule RepoMinerCore.Services.CodeRepoServiceTest do
  use RepoMinerCore.DataCase

  alias RepoMinerCore.Services.CodeRepoService
  alias RepoMinerCore.Models.{Branch, CommitDensity, Provider, Repository}

  describe "repositories" do
    import RepoMinerCoreWebWeb.CodeRepoServiceFixtures

    @invalid_attrs %{
      name: nil,
      token: nil,
      url: nil,
      visibility: nil
    }

    test "list_repositories/0 returns all repositories" do
      repository = repository_fixture()
      assert CodeRepoService.list_repositories() == [repository]
    end

    test "get_repository!/1 returns the repository with given id" do
      repository = repository_fixture()
      assert CodeRepoService.get_repository!(repository.id) == repository
    end

    test "create_repository/1 with valid data creates a repository" do
      valid_attrs = %{
        name: "some name",
        token: "some token",
        url: "some url",
        visibility: :public
      }

      assert {:ok, %Repository{} = repository} = CodeRepoService.create_repository(valid_attrs)
      assert repository.name == "some name"
      assert repository.token == "some token"
      assert repository.url == "some url"
      assert repository.visibility == :public
    end

    test "create_repository/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CodeRepoService.create_repository(@invalid_attrs)
    end

    test "update_repository/2 with valid data updates the repository" do
      repository = repository_fixture()

      update_attrs = %{
        name: "some updated name",
        token: "some updated token",
        url: "some updated url",
        visibility: :private
      }

      assert {:ok, %Repository{} = repository} =
               CodeRepoService.update_repository(repository, update_attrs)

      assert repository.name == "some updated name"
      assert repository.token == "some updated token"
      assert repository.url == "some updated url"
      assert repository.visibility == :private
    end

    test "update_repository/2 with invalid data returns error changeset" do
      repository = repository_fixture()

      assert {:error, %Ecto.Changeset{}} =
               CodeRepoService.update_repository(repository, @invalid_attrs)

      assert repository == CodeRepoService.get_repository!(repository.id)
    end

    test "delete_repository/1 deletes the repository" do
      repository = repository_fixture()
      assert {:ok, %Repository{}} = CodeRepoService.delete_repository(repository)
      assert_raise Ecto.NoResultsError, fn -> CodeRepoService.get_repository!(repository.id) end
    end

    test "change_repository/1 returns a repository changeset" do
      repository = repository_fixture()
      assert %Ecto.Changeset{} = CodeRepoService.change_repository(repository)
    end
  end

  describe "branches" do
    import RepoMinerCoreWebWeb.CodeRepoServiceFixtures

    test "list_branches/0 returns all branchs" do
      branch = branches_fixture()
      assert CodeRepoService.list_branches() == [branch]
    end

    test "get_branch/1 returns the branch with the given id" do
      branch = branches_fixture()
      assert CodeRepoService.get_branch!(branch.id) == branch
    end

    test "create_branch/1 with valid data creates a branch" do
      valid_attrs = %{
        name: "some name"
      }

      assert {:ok, %Branch{} = branch} = CodeRepoService.create_branch(valid_attrs)

      assert branch.name == "some name"
    end

    test "create_branch/1 with invalid data returns error" do
      invalid_attrs = %{name: nil}

      assert {:error, %Ecto.Changeset{}} = CodeRepoService.create_branch(invalid_attrs)
    end
  end

  describe "commit_densities" do
    import RepoMinerCoreWebWeb.CodeRepoServiceFixtures

    test "list_commit_desnties/0 returns all commitdensities" do
      commitdensity = commit_density_fixture()
      assert CodeRepoService.list_commit_densities() == [commitdensity]
    end

    test "get_commit_density/1 returns the branch with the given id" do
      commitdensity = commit_density_fixture()

      assert CodeRepoService.get_commit_density!(commitdensity.id) ==
               commitdensity
    end

    test "create_commit_density/1 with valid data creates a branch" do
      assert {:ok, %CommitDensity{} = commitdensity} =
               CodeRepoService.create_commit_density(%{
                 year: 2022,
                 month: 5,
                 no_commits: 12
               })

      assert commitdensity.year == 2022
      assert commitdensity.month == 5
      assert commitdensity.no_commits == 12
    end

    test "create_commit_density/1 with invalid data returns error" do
      invalid_attrs = %{
        year: nil,
        month: nil,
        no_commits: nil
      }

      assert {:error, %Ecto.Changeset{}} = CodeRepoService.create_commit_density(invalid_attrs)
    end
  end

  describe "Provider" do
    import RepoMinerCoreWebWeb.CodeRepoServiceFixtures

    test "list_provider/0 returns all providers" do
      provider = provider_fixture()
      assert CodeRepoService.list_providers() == [provider]
    end

    test "get_provider/1 returns the provider with the given id" do
      provider = provider_fixture
      assert CodeRepoService.get_provider!(provider.id) == provider
    end

    test "create_provider/1 with valid data creates a provider" do
      valid_attrs = %{
        name: "some name"
      }

      assert {:ok, %Provider{} = provider} = CodeRepoService.create_provider(valid_attrs)

      assert provider.name == "some name"
    end

    test "create_provider/1 with invalid data returns error" do
      invalid_attrs = %{name: nil}

      assert {:error, %Ecto.Changeset{}} = CodeRepoService.create_provider(invalid_attrs)
    end
  end
end
