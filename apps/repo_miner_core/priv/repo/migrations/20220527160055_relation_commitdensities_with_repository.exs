defmodule RepoMinerCore.Repo.Migrations.RelationCommitdensitiesWithRepository do
  use Ecto.Migration

  def up do
    alter table("commitdensities") do
      add :repository_id, references("repositories")
    end
  end

  def down do
    alter table("commitdensities") do
      remove :repository_id
    end
  end
end
