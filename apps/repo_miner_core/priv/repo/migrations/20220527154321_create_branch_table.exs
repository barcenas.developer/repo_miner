defmodule RepoMinerCore.Repo.Migrations.CreateBranchTable do
  use Ecto.Migration

  def up do
    create table("branches") do
      add :name, :string

      timestamps()
    end
  end

  def down do
    drop table("branches")
  end
end
