defmodule RepoMinerCore.Repo.Migrations.RelationRepositoriesWithProviders do
  use Ecto.Migration

  def up do
    alter table("providers") do
      add :repository_id, references("repositories")
    end
  end

  def down do
    alter table("providers") do
      remove :repository_id
    end
  end
end
