defmodule RepoMinerCore.Repo.Migrations.RelateUserWithRepository do
  use Ecto.Migration

  def up do
    alter table("repositories") do
      add :user_id, references("users")
    end
  end

  def down do
    alter table("repositories") do
      remove :user_id
    end
  end
end
