defmodule RepoMinerCore.Repo.Migrations.CreateProviderTable do
  use Ecto.Migration

  def up do
    create table("providers") do
      add :name, :string

      timestamps()
    end
  end

  def down do
    drop table("providers")
  end
end
