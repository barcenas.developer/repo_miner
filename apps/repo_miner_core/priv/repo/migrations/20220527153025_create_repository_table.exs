defmodule RepoMinerCore.Repo.Migrations.CreateRepositoryTable do
  use Ecto.Migration

  def up do
    create table("repositories") do
      add :name, :string
      add :url, :string
      add :visibility, :string
      add :token, :string
      add :no_commits, :integer
      add :status, :string

      timestamps()
    end
  end

  def down do
    drop table("repositories")
  end
end
