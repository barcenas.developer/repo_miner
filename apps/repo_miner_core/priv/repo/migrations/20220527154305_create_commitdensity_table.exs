defmodule RepoMinerCore.Repo.Migrations.CreateCommitdensityTable do
  use Ecto.Migration

  def up do
    create table("commitdensities") do
      add :name, :string
      add :year, :integer
      add :month, :integer
      add :no_commits, :integer

      timestamps()
    end
  end

  def down do
    drop table("commitdensities")
  end
end
