defmodule RepoMinerCore.Repo.Migrations.RelationProviderWithRepository do
  use Ecto.Migration

  def up do
    alter table("repositories") do
      add :provider_id, references("providers")
    end
  end

  def down do
    alter table("repositories") do
      remove :provider_id
    end
  end
end
