defmodule RepoMinerCore.Repo.Migrations.RelationBranchesWithRepository do
  use Ecto.Migration

  def up do
    alter table("branches") do
      add :repository_id, references("repositories")
    end
  end

  def down do
    alter table("branches") do
      remove :repository_id
    end
  end
end
