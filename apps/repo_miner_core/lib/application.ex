defmodule RepoMinerCore.Application do
  @moduledoc false

  use Application
  # alias RepoMinerCore.Config.Producer

  def start(_type, _args) do
    children = [
      RepoMinerCore.Repo
      # :poolboy.child_spec(Producer.pool_producer_name(), Producer.poolboy_config())
    ]

    opts = [strategy: :one_for_one, name: RepoMinerCore.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
