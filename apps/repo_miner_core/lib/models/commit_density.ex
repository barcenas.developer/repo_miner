defmodule RepoMinerCore.Models.CommitDensity do
  @moduledoc """
  Defines the schema of a CommitDensity
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "commitdensities" do
    field(:year, :integer)
    field(:month, :integer)
    field(:no_commits, :integer)

    belongs_to(:repository, RepoMinerCore.Models.Repository)

    timestamps()
  end

  @doc false
  def changeset(commitdensity, attrs) do
    commitdensity
    |> cast(attrs, [:year, :month, :no_commits])
    |> validate_required([:year, :month])
  end
end
