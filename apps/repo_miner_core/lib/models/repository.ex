defmodule RepoMinerCore.Models.Repository do
  @moduledoc """
  Defines the schema of a repository
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias RepoMinerCore.Models

  schema "repositories" do
    field(:name, :string)
    field(:url, :string)
    field(:visibility, Ecto.Enum, values: [:public, :private])
    field(:token, :string)
    field(:no_commits, :integer)
    field(:status, Ecto.Enum, values: [:succes, :error, :pending])

    belongs_to(:user, Models.User)
    has_one(:provider, Models.Provider, foreign_key: :repository_id)
    has_one(:commitdensity, Models.CommitDensity, foreign_key: :repository_id)
    has_many(:branches, Models.Branch, foreign_key: :repository_id)

    timestamps()
  end

  @doc false
  def changeset(repository, attrs) do
    repository
    |> cast(attrs, [:name, :url, :visibility, :token])
    |> validate_required([:name, :url, :visibility])
  end
end
