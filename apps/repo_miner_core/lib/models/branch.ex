defmodule RepoMinerCore.Models.Branch do
  @moduledoc """
  Defines the schema of a branch
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "branches" do
    field(:name, :string)
    # field(:no_commits, :integer)
    belongs_to(:repository, RepoMinerCore.Models.Repository, foreign_key: :repository_id)

    timestamps()
  end

  @doc false
  def changeset(branch, attrs) do
    branch
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
