defmodule RepoMinerCore.Models.User do
  @moduledoc """
  Defines the schema of a user
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field(:name, :string)
    field(:username, :string)
    field(:email, :string)
    field(:role, Ecto.Enum, values: [:admin, :collaborator])

    has_many(:repositories, RepoMinerCore.Models.Repository)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :username, :email, :role])
    |> validate_required([:name, :username, :email, :role])
  end
end
