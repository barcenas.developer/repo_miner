defmodule RepoMinerCore.Models.Provider do
  @moduledoc """
  Defines the schema of a Provider
  """
  use Ecto.Schema
  import Ecto.Changeset

  schema "providers" do
    field(:name, :string)

    belongs_to(:repository, RepoMinerCore.Models.Repository)

    timestamps()
  end

  @doc false
  def changeset(provider, attrs) do
    provider
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
