defmodule RepoMinerCore.Services.CodeRepoService do
  @moduledoc """
  The CodeRepoService context.
  """

  import Ecto.Query, warn: false
  alias RepoMinerCore.Repo
  alias RepoMinerCore.Models.{Branch, CommitDensity, Provider, Repository}

  @doc """
  CRUD for Branches
  """
  def create_branch(attrs \\ %{}) do
    %Branch{}
    |> Branch.changeset(attrs)
    |> Repo.insert()
  end

  def list_branches do
    Repo.all(Branch)
  end

  # def get_branch(id) do
  #   Repo.all(from(b in Branch, where: b.repository_id == ^id))
  # end

  def get_branch!(id) do
    Repo.get!(Branch, id)
  end

  @doc """
  CRUD for CommitDensity
  """

  def create_commit_density(attrs \\ %{}) do
    %CommitDensity{}
    |> CommitDensity.changeset(attrs)
    |> Repo.insert()
  end

  def list_commit_densities do
    Repo.all(CommitDensity)
  end

  # def get_commit_density(id) do
  #   Repo.all(from(c in CommitDensity, where: c.repository_id == ^id))
  # end

  def get_commit_density!(id) do
    Repo.get!(CommitDensity, id)
  end

  @doc """
  CRUD for provider
  """
  def create_provider(attrs \\ %{}) do
    %Provider{}
    |> Provider.changeset(attrs)
    |> Repo.insert()
  end

  def list_providers do
    Repo.all(Provider)
  end

  # def get_provider(id) do
  #   Repo.all(from(p in Provider, where: p.id == ^id))
  # end

  def get_provider!(id) do
    Repo.get!(Provider, id)
  end

  def list_repositories do
    Repo.all(Repository)
  end

  # def get_repository(id) do
  #   Repo.all(from(r in Repository, where: r.id == ^id))
  # end

  def get_repository!(id), do: Repo.get!(Repository, id)

  def create_repository(attrs \\ %{}) do
    %Repository{}
    |> Repository.changeset(attrs)
    |> Repo.insert()
  end

  def update_repository(%Repository{} = repository, attrs) do
    repository
    |> Repository.changeset(attrs)
    |> Repo.update()
  end

  def delete_repository(%Repository{} = repository) do
    Repo.delete(repository)
  end

  def change_repository(%Repository{} = repository, attrs \\ %{}) do
    Repository.changeset(repository, attrs)
  end

  # def list_analysed_repositories() do
  #   Repo.all(from(r in Repository, where: :status == :succes))
  # end

  # def list_unanalysed_repositories() do
  #   Repo.all(from(r in Repository, where: :status == :pending))
  # end

  # def request_repo_analysis(request) do
  #   # Join thr URL from the main form and send it to rabbitmq
  #   # The following code will be implemented when we want to send the request 
  #   case request do
  #     {:ok, miner} ->
  #       IO.puts("Request Accepted, analizing repo...")
  #       Process.sleep(10_000)
  #       miner

  #     {:error, error_msg} ->
  #       IO.puts("An error ocurred: #{error_msg}")

  #     _ ->
  #       nil
  #   end
  # end

  # def view_repo_analysis() do
  #   repo_name = Repo.get_by()
  # end
end
