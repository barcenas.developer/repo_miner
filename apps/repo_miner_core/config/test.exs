import Config

config :repo_miner_core, RepoMinerCore.Repo,
  username: "postgres",
  password: "postgres",
  database: "repo_miner_core_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox
