# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of the Config module.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
import Config

config :repo_miner_core, RepoMinerCore.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "repo_miner_core_repo_dev",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox,
  ownership_timeout: 1_000_000

config :repo_miner_core,
  ecto_repos: [RepoMinerCore.Repo]

import_config "#{config_env()}.exs"

defmodule RepoMinerCore.Config.Producer do
  @moduledoc false

  def pool_producer_name() do
    :repo_miner_producer
  end

  def poolboy_config do
    [
      name: {:local, pool_producer_name()},
      worker_module: RepoMiner.Producer,
      size: 10,
      max_overflow: 2
    ]
  end
end
