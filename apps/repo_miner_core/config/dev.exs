import Config

# Configure your database
config :repo_miner_core, RepoMinerCore.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "repo_miner_core_dev",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

config :repo_miner_core,
  ecto_repos: [RepoMinerCore.Repo]
