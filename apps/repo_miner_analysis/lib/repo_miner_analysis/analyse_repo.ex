defmodule RepoMinerAnalysis.AnalyzeRepository do
  @moduledoc """
  Module that parses a repository
  """
  alias RepoMinerPy
  use GenServer

  def init(init_arg) do
    {:ok, init_arg}
  end

  def repo_miner(repo_url) do
    {:ok, miner} = RepoMinerPy.RepoMiner.start_link(name: MyRepoMiner)
    {:ok, repo_info} = GenServer.call(miner, {:analyze, repo_url: repo_url, token: nil}, 60_000)
    repo_info
  end
end
